
package cardgame;
import java.util.*;

public class CardGame {
    
    public static void main(String[] args){
        
        int dealer_Val=0;
        int user1_Value=0;
        int user2_Value=0;
        
        Scanner in = new Scanner(System.in);
        CardHandGenerator generator = new CardHandGenerator();
        
        Card[] cardHand = generator.generateHand(7);
        Random random = new Random();
        
        for(int i=0;i<cardHand.length;i++){
          
            int value = random.nextInt(12)+1;
            String suit = Card.SUITS[random.nextInt(3)];
            
           
            Card card = new Card(value, suit);
            cardHand[i] = card;
        }
        System.out.println("Here are the Cards for User 1: ");
        for(int i=0; i<cardHand.length;i++){
            System.out.println(cardHand[i].getValue() + " of " + cardHand[i].getSuit());
        
    }
         System.out.println("Here are the Cards for User 2: ");
        for(int i=0; i<cardHand.length;i++){
            System.out.println(cardHand[i].getValue() + " of " + cardHand[i].getSuit());      
    }
        BlackJackBrain blackJackBrain = new BlackJackBrain(cardHand);
        
        
        
}

}



