package cardgame;

import java.util.Random;

public class CardHandGenerator {

	/**
	 * 
	 * @param numCards
	 */
	public Card[] generateHand(int numCards) {
		Card[] cardHand = new Card[numCards];
        Random random = new Random();
        
        for(int i=0;i<cardHand.length;i++){
            //getting a random value from 1-12
            int value = random.nextInt(12)+1;
            String suit = Card.SUITS[random.nextInt(3)];
            
            //Create an instance of card and assign it to the array
            Card card = new Card(value, suit);
            cardHand[i] = card;
         }
        return cardHand;
	}

}